import _ from 'lodash';

import './index.scss';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

function ready() {

	let appDiv = document.createElement('div');
	appDiv.id = "app";
	document.body.appendChild(appDiv);
	appDiv.innerHTML = `Введите дату : с <span id="spanFrom"> </span> до <span id="spanTo"> </span> `;

	//Два дейт-пикера: "Начало периода" и "Конец периода" , кнопка отправки
	let input_from = document.createElement('input');
	input_from.id = "tsFrom";
	input_from.placeholder = `дата с`;
	input_from.type = "date";
	document.getElementById("spanFrom").appendChild(input_from);
	//appDiv.appendChild(input_from);	
	
	let input_to = document.createElement('input');
	input_to.id = "tsTo";
	input_to.placeholder = `дата по`; 
	input_to.type = "date";
	document.getElementById("spanTo").appendChild(input_to);
	//appDiv.appendChild(input_to);	
	
	let input_button_from_to = document.createElement('input');
	input_button_from_to.type = "button";
	input_button_from_to.value = "Отправить";
	input_button_from_to.onclick = function () {
		//hide(arrow.id,m_select_from.id,data_from.id);	
		take_inform();
	}
	appDiv.appendChild(input_button_from_to);
	
	let oOption = document.createElement('select');
		oOption.id = "oOption";					
		/*oOption.onchange = function () { 		
			OnSelectionChange(this.value);
		}*/									
		//oOption.className = 'oOption';		
		appDiv.appendChild(oOption);
						
	let severalgraphics = document.createElement('div');
		severalgraphics.id = "severalgraphics";
		severalgraphics.className = 'severalgraphics';		
		appDiv.appendChild(severalgraphics);



	function take_inform() {
		var date_from = document.getElementById('tsFrom').value;
		var date_to = document.getElementById('tsTo').value;
		//console.log('c',date_from,'по',date_to);

		var date_otpr = {};
		date_otpr.tsFrom = date_from;
		date_otpr.tsTo = date_to;
		//console.log(date_otpr);

		take_inform_page(date_otpr);
		
	}


	function take_inform_page(date_otpr) {
		//console.log(date_otpr);
		//console.log(date_otpr.tsFrom);

		var request = new XMLHttpRequest();
		request.open("GET", "https://gowifi.herokuapp.com/api/frequenciesPeriod?tsFrom=" + date_otpr.tsFrom + "&tsTo=" + date_otpr.tsTo, true);

		request.onload = function () {
			if (request.status >= 200 && request.status < 400) {
				var data = JSON.parse(request.responseText);
				console.log(data);
				data = data.data;

				if (data != null && data !== "") {
					//obj_source = {};
					var arr_source_comp = [];
					var obj_source = {};
					for (var key in data) {

						var cur = data[key];
						var cur_source = data[key].source;
						
						//if (cur_source == "") {cur_source = "null";}
						
						if (cur_source !== "") {
							if (typeof obj_source[cur_source] !== "undefined") {
								//ключ есть
								//arr_source_comp.push(cur);	
								obj_source[cur_source].push(cur);
								obj_source[cur_source] = obj_source[cur_source];

							} else {
								//ключа нет
								arr_source_comp = [];
								arr_source_comp.push(cur);
								obj_source[cur_source] = arr_source_comp;

							}
						} else {
							console.log('Нет source');
						}

					}
				} else {
					console.log('Нет данных');
				};
				//console.log(arr_source_comp);	
				//console.log(obj_source);	
				
				document.getElementById('severalgraphics').innerHTML = ""; // очищаем поле графика
				document.getElementById('oOption').innerHTML = ""; // очищаем поле графика
				
								
				var objSel = document.getElementById("oOption");	
					objSel.options.length = 0;					
								
				for (var key in obj_source) {					
					objSel.options[objSel.options.length] = new Option(key, key);	
					//console.log(key);	//console.log(obj_source[key]);
					//var resurs = obj_source[key];
					//grafik_blok(key, resurs);
				}				
				
				var keys = Object.keys(obj_source); //получаем ключи объекта в виде массива
					//console.log(keys[0],obj_source[keys[0]]); // первый элемент
				var resurs = obj_source[keys[0]];
					grafik_blok(keys[0], resurs);				
				document.getElementById("oOption").onchange = function() {myFunction(obj_source)}; // проеряем на изменения								

			} else {
				// error
				document.getElementById('severalgraphics').innerHTML = ""; // очищаем поле графика
			}
		};

		request.send();

		/*
		date_otpr = JSON.stringify(date_otpr);
		var myHeaders = new Headers({
			"Content-Type": "application/json",
		});
		var url = "http:/9wRgDvn";
		fetch(url, {
			method: 'POST', // or 'PUT'
			body: date_otpr,
			headers: myHeaders
		})
		.then(res => res.json())
		.catch(error => console.error('Error:', error))
		.then(function (jsonAnswer) {
			console.log('jsonAnswer',jsonAnswer);
			//var take_inform_all = take_inform_page_in(jsonAnswer);
			//Table(take_inform_all);
		});
		*/
	};

	//grafik_blok();
	function myFunction(obj_source) {
				
	  document.getElementById('severalgraphics').innerHTML = ""; // очищаем поле графика
	  var x = document.getElementById("oOption");
	  //console.log(x.value);	
	  //console.log(obj_source[x.value]);	
	  //x.value = x.value.toUpperCase();
	  
	  var resurs = obj_source[x.value];
		  grafik_blok(x.value, resurs);
	};
		
	
	function grafik_blok(key, resurs) {

		console.log(key, resurs);
		//document.getElementById('severalgraphics').innerHTML = ""; // очищаем поле графика
		// блок графика
		let chartdiv = document.createElement('div');
		//chartdiv.id = "chartdiv";
		chartdiv.className = 'chartdiv';
		chartdiv.id = key;
		severalgraphics.appendChild(chartdiv);

		// Themes begin
		//am4core.useTheme(am4themes_animated);
		// Themes end

		// Create chart instance
		//var chart = am4core.create("chartdiv", am4charts.XYChart3D);
		var chart = am4core.create(key, am4charts.XYChart3D);

		// Add data

		chart.data = resurs;

		// Create axes
		var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		//categoryAxis.dataFields.category = "ts_from";
		categoryAxis.dataFields.category = "ts_from";
		categoryAxis.title.text = "Соотношение уникальных фиксаций к общему количеству за час";
		categoryAxis.renderer.grid.template.location = 0;
		//categoryAxis.renderer.minGridDistance = 30;
		categoryAxis.renderer.labels.template.rotation = -45;
		categoryAxis.renderer.labels.template.verticalCenter = "middle";		
		categoryAxis.renderer.labels.template.horizontalCenter = "right";
		categoryAxis.startLocation = 0;
		//categoryAxis.endLocation = 0;



		var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		//valueAxis.title.text = "ts_from";
		valueAxis.title.text = key;

		//valueAxis.renderer.labels.template.adapter.add("text", function(text) { return text + "%";});


		// Add vertical scrollbar
		// chart.scrollbarY = new am4core.Scrollbar();
		// chart.scrollbarY.marginLeft = 0;
		// chart.scrollbarX = new am4core.Scrollbar();
		// chart.scrollbarX.marginLeft = 0;

		// Add cursor
		chart.cursor = new am4charts.XYCursor();
		/*chart.cursor.behavior = "zoomX";
		chart.cursor.lineY.disabled = true;*/


		// Create series
		var dataFields_valueY, dataFields_categoryX, series_name, am4core_color, template_fillOpacity;

		dataFields_valueY = "cnt_un";
		dataFields_categoryX = "ts_from";
		series_name = "Уникальные фиксации";
		am4core_color = "#8B008B";
		// template_fillOpacity =  0.9;
		// var template.tooltipText = ;			 


		series(dataFields_valueY, dataFields_categoryX, series_name, am4core_color, chart, template_fillOpacity);

		dataFields_valueY = "cnt_all";
		dataFields_categoryX = "ts_from";
		series_name = "Все фиксации";
		am4core_color = "green";
		// template_fillOpacity =  0.9;
		// var template.tooltipText = ;
		//series.columns.template.fillOpacity = 0.9;

		series(dataFields_valueY, dataFields_categoryX, series_name, am4core_color, chart, template_fillOpacity);
	};

	function series(dataFields_valueY, dataFields_categoryX, series_name, am4core_color, chart, template_fillOpacity) {
		var series = chart.series.push(new am4charts.ColumnSeries3D());
		series.dataFields.valueY = dataFields_valueY;
		series.dataFields.categoryX = dataFields_categoryX;
		series.name = series_name;
		//series.fill = am4core.color(am4core_color);
		series.clustered = false;
		series.columns.template.tooltipText = " " + series_name + ": [bold]{valueY}[/]";
		//series.columns.template.fillOpacity = template_fillOpacity;
	};


}

document.addEventListener("DOMContentLoaded", ready);
//}
